# vmail-manage – A terminal management interface for my mail server

This is supposed to be(come) software for me to manage accounts, aliases and policies for my MTA.
The software just has to talk with the MariaDB server, so probably a website would have been easier,
but well... I've always wanted to build a CLI and a TUI in my life. And I wanted to build something
for production use written in C++. So here you go – a CLI and TUI for interacting with a database.
Something that could have easily been done using some shell magic or a stupid website, but shall
be(come) a terminal application written in C++ with it's own manpage. Have I mentioned that I've
always wanted to write a manpage for use in production? No? Okay, now I have.

## License

Copyright © 2020 Adrian Schollmeyer. All rights reserved.

An if I finish the project: Probably GPLish. Probably GPLv3. Or maybe AGPLv3.

## Installation

It's just CMake-stuff, so just use the duo infernale (aka `cmake .. && make`):

	$ git clone https://gitlab.com/nexadn/vmail-manage.git
	$ mkdir vmail-manage/cli/build
	$ cd vmail-manage/cli/build
	$ cmake .. -DCMAKE_BUILD_TYPE=Release
	$ make -j
	$ sudo make install

But I haven't testet installing it yet as it's not doing anything useful at the moment.

## Usage

Have a look at the manpages. In the Repo:

	$ cd man
	$ man -l vmail-manage.man
