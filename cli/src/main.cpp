#include <iostream>

#include <unistd.h>

#include "Cli.hpp"
#include "VMail.hpp"

namespace
{
constexpr const char* getopt_opts{"ct"};

constexpr const char* DB_USER{"vmail"};
constexpr const char* DB_PASSWORD{"vmail"};
constexpr const char* DB_SERVER{"localhost"};
constexpr const char* DB_DATABSE{"vmail"};

bool action_cli{false};
bool action_tui{false};
} // namespace

int main(int argc, char** argv)
{
	int opt;
	while ((opt = getopt(argc, argv, getopt_opts)) != -1) {
		switch (opt) {
		case 'c':
			action_cli = true;
			break;
		case 't':
			action_tui = true;
			break;
		default:
			std::cerr << "Unrecognized option: " << opt << "/" << static_cast<char>(opt) << "\n";
			return 1;
		}
	}

	if (action_cli && action_tui) {
		std::cerr << "Too many options specified\n";
		return 1;
	} else if (!action_cli && !action_tui) {
		std::cerr << "No options specified\n";
		return 1;
	}

	if (action_cli) {
		VMail vmail(DB_USER, DB_PASSWORD, DB_DATABSE, DB_SERVER);
		Cli cli(vmail);
		cli.run_cli();
	} else if (action_tui) {
		std::cerr << "wait4feature\n";
		return 1;
	} else {
		std::cerr << "Unexpected error. No options given\n";
		return 1;
	}
}
