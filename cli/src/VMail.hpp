#pragma once

#include <string>
#include <vector>

#include "pch_sqlpp11.hpp"

#include "models/VMailModels.hpp"

class VMail
{
private:
	std::unique_ptr<sqlpp::mysql::connection> connection;

public:
	VMail(const std::string& db_user, const std::string& db_password,
	      const std::string& db_name = "vmail", const std::string& db_server = "localhost",
	      const unsigned int port = 3306);
	VMail(const VMail&) = delete;
	VMail& operator=(const VMail&) = delete;
	~VMail();

	inline void start_transaction()
	{
		connection->start_transaction();
	}
	inline void commit_transaction()
	{
		connection->commit_transaction();
	}
	inline void rollback_transaction(bool report = false)
	{
		connection->rollback_transaction(report);
	}

	inline models::Account get_account(const std::string& name, const models::Domain& domain) const
	{
		return get_account(name, domain.domain());
	}
	models::Account get_account(const std::string& name, const std::string& domain) const;

	models::Domain get_domain(const size_t id) const;
	models::Domain get_domain(const std::string& name) const;

	inline std::vector<models::Alias>
	get_aliases_by_source(const std::string& source_name, const models::Domain& source_domain) const
	{
		return get_aliases_by_source(source_name, source_domain.domain());
	}
	std::vector<models::Alias> get_aliases_by_source(const std::string& source_name,
	                                                 const std::string& source_domain) const;

	std::vector<models::Alias> get_aliases_by_destination(const std::string& dest_name,
	                                                      const std::string& dest_domain) const;

	models::Account create_account(const std::string& name, const models::Domain& domain,
	                               const std::string& password) const;
	models::Domain create_domain(const std::string& domain) const;
	models::Alias create_alias(const std::string& source_name, const models::Domain& source_domain,
	                           const std::string& dest_name, const std::string& dest_domain) const;

	template <typename T>
	void update(const T& item) const;

	void update_account(const models::Account& account) const;
};
