#pragma once

#include <istream>
#include <string>
#include <vector>

namespace Util
{
std::vector<std::string> split(const std::string& input_string, char split_char = ' ');
std::vector<std::string> split(std::istream& input_stream, char split_char = ' ');
} // namespace Util
