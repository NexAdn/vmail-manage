#pragma once

#include <functional>

template <typename T, typename UpdateFunction = std::function<void(const T&)>>
class NotifyWrapper
{
private:
	T& value;
	UpdateFunction updater;

public:
	NotifyWrapper(
	    T& wrapped_value, const UpdateFunction& updater = [](const T&) {})
	    : value(wrapped_value), updater(updater)
	{}

	~NotifyWrapper()
	{}

	NotifyWrapper& operator=(const T& rhs_value)
	{
		value = rhs_value;
		updater(value);

		return *this;
	}

	operator const T&()
	{
		return value;
	}
};
