#include <sstream>
#include <string>

#include "Util.hpp"

namespace Util
{
std::vector<std::string> split(const std::string& input_string, char split_char)
{
	std::istringstream input_stream{input_string};
	return split(input_stream, split_char);
}

std::vector<std::string> split(std::istream& input_stream, char split_char)
{
	std::vector<std::string> output_list;
	std::string current_segment;

	while (std::getline(input_stream, current_segment, split_char)) {
		if (current_segment != "") {
			output_list.push_back(current_segment);
		}
	}

	output_list.shrink_to_fit();

	return output_list;
}
} // namespace Util
