#include <stdexcept>

#include <sqlpp11/mysql/connection_config.h>

#include "VMail.hpp"
#include "vmail_sql.h"

namespace
{
SQLPP_ALIAS_PROVIDER(a_account_id);
SQLPP_ALIAS_PROVIDER(a_domain_id);
SQLPP_ALIAS_PROVIDER(a_account_domain);
SQLPP_ALIAS_PROVIDER(a_domain_domain);
SQLPP_ALIAS_PROVIDER(a_alias_id);
} // namespace

VMail::VMail(const std::string& username, const std::string& password, const std::string& database,
             const std::string& server, const unsigned int port)
{
	auto conf = std::make_shared<sqlpp::mysql::connection_config>();
	conf->user = username;
	conf->password = password;
	conf->database = database;
	conf->host = server;
	conf->port = port;

	connection = std::make_unique<sqlpp::mysql::connection>(conf);
}

VMail::~VMail()
{}

models::Account VMail::get_account(const std::string& name, const std::string& domain) const
{
	tbl::Accounts accounts;
	tbl::Domains domains;
	auto res = (*connection)(
	    select(accounts.id.as(a_account_id), accounts.domain.as(a_account_domain),
	           accounts.username, accounts.password, accounts.quota, accounts.enabled,
	           accounts.sendonly, domains.id.as(a_domain_id), domains.domain.as(a_domain_domain))
	        .from(accounts.inner_join(domains).on(accounts.domain == domains.domain))
	        .where(accounts.username == name && accounts.domain == domain));

	for (const auto& row : res) {
		models::Domain domain(*this, row.a_domain_id, row.a_domain_domain);
		models::Account account(*this, row.a_account_id, row.username, domain, row.password,
		                        row.quota, row.enabled, row.sendonly);
		return account;
	}

	throw std::runtime_error("No account found");
}

models::Domain VMail::get_domain(const size_t id) const
{
	tbl::Domains domains;
	auto res = (*connection)(select(all_of(domains)).from(domains).where(domains.id == id));
	for (const auto& row : res) {
		models::Domain domain(*this, row.id, row.domain);
		return domain;
	}

	throw std::runtime_error("No domain found");
}

models::Domain VMail::get_domain(const std::string& name) const
{
	tbl::Domains domains;
	auto res = (*connection)(select(all_of(domains)).from(domains).where(domains.domain == name));

	for (const auto& row : res) {
		models::Domain domain(*this, row.id, row.domain);
		return domain;
	}

	throw std::runtime_error("No domain found");
}

std::vector<models::Alias> VMail::get_aliases_by_source(const std::string& source_username,
                                                        const std::string& source_domain) const
{
	tbl::Aliases aliases;
	tbl::Domains domains;
	auto res = (*connection)(
	    select(aliases.id.as(a_alias_id), aliases.sourceUsername, aliases.sourceDomain,
	           aliases.destinationUsername, aliases.destinationDomain, aliases.enabled,
	           domains.id.as(a_domain_id))
	        .from(aliases.inner_join(domains).on(aliases.sourceDomain == domains.domain))
	        .where(aliases.sourceUsername == source_username
	               && aliases.sourceDomain == source_domain));

	std::vector<models::Alias> res_aliases;
	for (const auto& row : res) {
		models::Domain domain(*this, row.a_domain_id, row.sourceDomain);
		res_aliases.push_back({*this, row.a_alias_id, row.sourceUsername, domain,
		                       row.destinationUsername, row.destinationDomain,
		                       (row.enabled > 0) ? true : false});
	}

	return res_aliases;
}

std::vector<models::Alias>
VMail::get_aliases_by_destination(const std::string& destination_username,
                                  const std::string& destination_domain) const
{
	tbl::Aliases aliases;
	tbl::Domains domains;
	auto res = (*connection)(
	    select(aliases.id.as(a_alias_id), aliases.sourceUsername, aliases.sourceDomain,
	           aliases.destinationUsername, aliases.destinationDomain, aliases.enabled,
	           domains.id.as(a_domain_id))
	        .from(aliases.inner_join(domains).on(aliases.sourceDomain == domains.domain))
	        .where(aliases.destinationUsername == destination_username
	               && aliases.destinationDomain == destination_domain));

	std::vector<models::Alias> res_aliases;
	for (const auto& row : res) {
		models::Domain domain(*this, row.a_domain_id, row.sourceDomain);
		res_aliases.push_back({*this, row.a_alias_id, row.sourceUsername, domain,
		                       row.destinationUsername, row.destinationDomain,
		                       (row.enabled > 0) ? true : false});
	}

	return res_aliases;
}

models::Account VMail::create_account(const std::string& name, const models::Domain& domain,
                                      const std::string& password) const
{
	tbl::Accounts accounts;
	tbl::Domains domains;
	auto insert_res = (*connection)(insert_into(accounts).set(
	    accounts.username = name, accounts.domain = domain.domain(), accounts.password = password));
	auto select_res = (*connection)(
	    select(accounts.id.as(a_account_id), accounts.domain.as(a_account_domain),
	           accounts.username, accounts.password, accounts.quota, accounts.enabled,
	           accounts.sendonly, domains.id.as(a_domain_id), domains.domain.as(a_domain_domain))
	        .from(accounts.inner_join(domains).on(accounts.domain == domains.domain))
	        .where(accounts.id == insert_res));

	for (const auto& row : select_res) {
		models::Domain domain(*this, row.a_domain_id, row.a_domain_domain);
		models::Account account(*this, row.a_account_id, row.username, domain, row.password,
		                        row.quota, row.enabled, row.sendonly);
		return account;
	}

	throw std::runtime_error("Unexpected error\n");
}

models::Domain VMail::create_domain(const std::string& domain) const
{
	tbl::Domains domains;
	auto insert_res = (*connection)(insert_into(domains).set(domains.domain = domain));

	return get_domain(insert_res);
}

models::Alias VMail::create_alias(const std::string& source_name,
                                  const models::Domain& source_domain, const std::string& dest_name,
                                  const std::string& dest_domain) const
{
	tbl::Aliases aliases;
	tbl::Domains domains;
	auto insert_res = (*connection)(insert_into(aliases).set(
	    aliases.sourceUsername = source_name, aliases.sourceDomain = source_domain.domain(),
	    aliases.destinationUsername = dest_name, aliases.destinationDomain = dest_domain));

	auto select_res = (*connection)(
	    select(aliases.id.as(a_alias_id), aliases.sourceUsername, aliases.sourceDomain,
	           aliases.destinationUsername, aliases.destinationDomain, aliases.enabled,
	           domains.id.as(a_domain_id))
	        .from(aliases.inner_join(domains).on(aliases.sourceDomain == domains.domain))
	        .where(aliases.id == insert_res));

	for (const auto& row : select_res) {
		models::Domain domain(*this, row.a_domain_id, row.sourceDomain);
		models::Alias alias(*this, row.a_alias_id, row.sourceUsername, domain,
		                    row.destinationUsername, row.destinationDomain, row.enabled);
		return alias;
	}

	throw std::runtime_error("Unexpected error\n");
}

template <>
void VMail::update<models::Account>(const models::Account& account) const
{
	update_account(account);
}

void VMail::update_account(const models::Account& account) const
{
	tbl::Accounts accounts;
	(*connection)(sqlpp::update(accounts)
	                  .set(accounts.username = account.username(),
	                       accounts.password = account.password(), accounts.quota = account.quota(),
	                       accounts.enabled = account.enabled() ? 1 : 0,
	                       accounts.sendonly = account.sendonly() ? 1 : 0)
	                  .where(accounts.id == account.id()));
}
