#include <sstream>

#include "CommandTree.hpp"

CommandTree::~CommandTree()
{}

void CommandTree::add_child(std::shared_ptr<CommandTree> child)
{
	if (children.size() == 1
	    && dynamic_cast<CommandTreeParameter*>(children.at(0).get()) != nullptr) {
		throw CommandTreeException("CommandTreeParameter must be an only child");
	} else {
		children.push_back(child);
	}
}

CommandTreeAction::CommandTreeAction(const std::string& label)
{
	this->label = label;
}

CommandTreeAction::CommandTreeAction(const std::string& label, executor_t executor)
    : CommandTreeAction(label)
{
	this->executor = executor;
}

CommandTreeAction::~CommandTreeAction()
{}

std::string CommandTreeAction::get_syntax() const
{
	std::ostringstream buf;
	buf << label;
	switch (children.size()) {
	case 0:
		break;
	case 1:
		buf << ' ';
		break;
	default:
		buf << " [";
		break;
	}

	bool first_child_done{false};
	for (auto& child : children) {
		if (first_child_done) {
			buf << '|';
		} else {
			first_child_done = true;
		}
		buf << child->get_syntax();
	}

	if (children.size() > 1) {
		buf << ']';
	}

	return buf.str();
}

void CommandTree::execute(const std::vector<std::string>& input) const
{
	argument_map_t collected_args;
	auto input_iterator = input.begin();
	auto input_end = input.end();
	if (input_iterator == input_end) {
		throw CommandTreeException("Empty input");
	} else if (*input_iterator != label) {
		throw CommandTreeException("Invalid input. Syntax:\n" + get_syntax());
	}
	input_iterator++;
	execute(input_iterator, input_end, collected_args);
}

std::string CommandTree::get_label() const
{
	return label;
}

bool CommandTree::is_leaf() const
{
	return children.size() == 0;
}

bool CommandTree::has_parameter() const
{
	if (dynamic_cast<const CommandTreeParameter*>(this) != nullptr) {
		return true;
	}

	if (children.size() == 1) {
		return dynamic_cast<const CommandTreeParameter*>(children.at(0).get()) != nullptr;
	}

	return false;
}

bool CommandTreeAction::execute(vector_iterator_t input_iterator, vector_iterator_t input_end,
                                argument_map_t& collected_arguments) const
{
	if (is_leaf()) {
		executor(collected_arguments);
	} else if (has_parameter()) {
		children.at(0)->execute(input_iterator, input_end, collected_arguments);
		if (children.at(0)->is_leaf()) {
			executor(collected_arguments);
		}
	} else {
		if (input_iterator == input_end) {
			throw CommandTreeException("Input too short. Syntax:\n" + get_syntax());
		}
		std::string next_command = *input_iterator;
		input_iterator++;
		for (auto& child : children) {
			if (child->get_label() == next_command) {
				child->execute(input_iterator, input_end, collected_arguments);
			}
		}
	}
	return true;
}

CommandTreeParameter::CommandTreeParameter(const std::string& label)
{
	this->label = label;
}

CommandTreeParameter::~CommandTreeParameter()
{}

std::string CommandTreeParameter::get_syntax() const
{
	std::ostringstream buf;
	buf << '<' << label << '>';
	return buf.str();
}

bool CommandTreeParameter::execute(vector_iterator_t input_iterator, vector_iterator_t input_end,
                                   argument_map_t& collected_arguments) const
{
	std::string value = *input_iterator;
	input_iterator++;
	collected_arguments.insert_or_assign(label, value);

	if (is_leaf()) {
		return true;
	} else {
		if (children.size() == 1
		    && dynamic_cast<CommandTreeParameter*>(children.at(0).get()) != nullptr) {
			return children.at(0)->execute(input_iterator, input_end, collected_arguments);
			// TODO: Bug: Two parameters one after another stop execution silently
		} else {
			std::string next_label = *input_iterator;
			input_iterator++;
			for (auto& child : children) {
				if (child->get_label() == next_label) {
					return child->execute(input_iterator, input_end, collected_arguments);
				}
			}
		}
	}

	return false;
}

CommandTreeException::CommandTreeException(const std::string& reason) : reason(reason)
{}

CommandTreeException::~CommandTreeException()
{}

const char* CommandTreeException::what() const noexcept
{
	return reason.c_str();
}
