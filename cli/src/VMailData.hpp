#pragma once

#include <cstddef>
#include <map>
#include <string>

#include "NotifyWrapper.hpp"

class VMailData
{
public:
	value_t = std::string;

private:
	std::map<std::string, NotifyWrapper<value_t>> data;

public:
	VMailData();
	virtual ~VMailData();

	NotifyWrapper<value_t>& operator[](const std::string& key);
};
