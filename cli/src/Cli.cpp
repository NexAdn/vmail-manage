#include <iostream>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <unordered_map>
#include <vector>

#include <readline/history.h>
#include <readline/readline.h>

#include "Cli.hpp"
#include "CommandTree.hpp"
#include "Util.hpp"
#include "VMail.hpp"

Cli::Cli(VMail& vmail) : vmail(vmail)
{}

Cli::~Cli()
{}

void Cli::run_cli()
{
	if (command_trees.size() == 0) {
		setup_command_trees();
	}

	while (run) {
		const char* input_raw = readline(get_prompt());

		std::vector<std::string> split_command_line = Util::split(input_raw);
		try {
			const std::string& command = split_command_line.at(0);

			command_trees.at(command)->execute(split_command_line);
		}
		catch (std::exception& e) {
			cli_error("Invalid command\n");
			print_usage();
		}
	}
}

const char* Cli::get_prompt() const
{
	return "vmail> ";
}

void Cli::cli_error(const std::string& msg) const
{
	std::cout << "[E] " + msg;
}

void Cli::cli_info(const std::string& msg) const
{
	std::cout << "[i] " + msg;
}

void Cli::cli_success(const std::string& msg) const
{
	std::cout << "[*] " + msg;
}

void Cli::setup_command_trees()
{
	std::unique_ptr<CommandTree> create{new CommandTreeAction("create")};

	std::shared_ptr<CommandTree> create_alias{
	    new CommandTreeAction("alias", [this](std::unordered_map<std::string, std::string> args) {
		    try {
			    auto alias_addr_parts = Util::split(args.at("alias"), '@');
			    auto target_addr_parts = Util::split(args.at("target"), '@');

			    vmail.start_transaction();

			    models::Domain alias_domain = vmail.get_domain(alias_addr_parts.at(1));
			    models::Alias alias =
			        vmail.create_alias(alias_addr_parts.at(0), alias_domain,
			                           target_addr_parts.at(0), target_addr_parts.at(1));

			    vmail.commit_transaction();

			    std::stringstream success_msg;
			    success_msg << "Successfully created alias " << alias.id() << ": "
			                << args.at("alias") << " -> " << args.at("target") << std::endl;
			    cli_success(success_msg.str());
		    }
		    catch (std::out_of_range&) {
			    cli_error("Invalid E-Mail-Address!\n");
		    }
		    catch (std::runtime_error&) {
			    cli_error("Couldn't create alias!\n");
		    }
	    })};
	std::shared_ptr<CommandTree> create_alias_name{new CommandTreeParameter("alias")};
	std::shared_ptr<CommandTree> create_alias_name_target{new CommandTreeParameter("target")};
	create_alias_name->add_child(create_alias_name_target);
	create_alias->add_child(create_alias_name);
	create->add_child(create_alias);

	std::shared_ptr<CommandTree> create_user{
	    new CommandTreeAction("account", [this](std::unordered_map<std::string, std::string> args) {
		    try {
			    auto addr_parts = Util::split(args.at("address"), '@');

			    vmail.start_transaction();

			    // TODO
			    models::Account acc = vmail.create_account(
			        addr_parts.at(0), vmail.get_domain(addr_parts.at(1)), "fail");
			    const auto& c_acc{acc};

			    vmail.commit_transaction();

			    cli_success("Successfully created account " + c_acc.username() + '@'
			                + c_acc.domain().domain() + " with ID " + std::to_string(c_acc.id())
			                + '@' + std::to_string(c_acc.domain().id()) + '\n');
		    }
		    catch (std::out_of_range&) {
			    cli_error("Invalid arguments!\n");
		    }
	    })};
	std::shared_ptr<CommandTree> create_user_name{new CommandTreeParameter("address")};
	create_user->add_child(create_user_name);
	create->add_child(create_user);

	std::unique_ptr<CommandTree> del{new CommandTreeAction("delete")};

	std::shared_ptr<CommandTree> del_user{new CommandTreeAction("account")};
	std::shared_ptr<CommandTree> del_user_name{new CommandTreeParameter("address")};
	del_user->add_child(del_user_name);
	del->add_child(del_user);

	std::shared_ptr<CommandTree> del_alias{new CommandTreeAction("alias")};
	std::shared_ptr<CommandTree> del_alias_name{new CommandTreeParameter("alias")};
	std::shared_ptr<CommandTree> del_alias_name_all{new CommandTreeAction("all")};
	std::shared_ptr<CommandTree> del_alias_name_single{new CommandTreeAction("single")};
	std::shared_ptr<CommandTree> del_alias_name_single_target{new CommandTreeParameter("target")};
	del_alias_name_single->add_child(del_alias_name_single_target);
	del_alias_name->add_child(del_alias_name_all);
	del_alias_name->add_child(del_alias_name_single);
	del_alias->add_child(del_alias_name);
	del->add_child(del_alias);

	std::unique_ptr<CommandTree> show{new CommandTreeAction("show")};
	std::shared_ptr<CommandTree> show_user{
	    new CommandTreeAction("account", [this](std::unordered_map<std::string, std::string> args) {
		    auto mail_parts = Util::split(args.at("E-Mail"), '@');
		    try {
			    const auto account = vmail.get_account(mail_parts.at(0), mail_parts.at(1));
			    std::cout << "Address:  " << account.username() << '@' << account.domain().domain()
			              << std::endl
			              << "ID:       " << account.id() << '@' << account.domain().id()
			              << std::endl
			              << "Quota:    " << account.quota() << std::endl;

			    if (!account.enabled()) {
				    cli_info("This account is disabled!\n");
			    }
			    if (account.sendonly()) {
				    cli_info("This account is sendonly!\n");
			    }
		    }
		    catch (std::out_of_range&) {
			    cli_error("Invalid E-Mail-Adress!\n");
		    }
		    catch (std::runtime_error&) {
			    cli_error("Account not found!\n");
		    }
	    })};
	show->add_child(show_user);
	std::shared_ptr<CommandTree> show_user_email{new CommandTreeParameter("E-Mail")};
	show_user->add_child(show_user_email);
	std::shared_ptr<CommandTree> show_domain{
	    new CommandTreeAction("domain", [this](std::unordered_map<std::string, std::string> args) {
		    try {
			    const auto domain = vmail.get_domain(args.at("domain"));
			    std::cout << "ID:   " << domain.id() << std::endl
			              << "Name: " << domain.domain() << std::endl;
		    }
		    catch (std::runtime_error&) {
			    cli_error("Domain not found!\n");
		    }
	    })};
	show->add_child(show_domain);
	std::shared_ptr<CommandTree> show_domain_name{new CommandTreeParameter("domain")};
	show_domain->add_child(show_domain_name);
	std::shared_ptr<CommandTree> show_alias{new CommandTreeAction("alias")};
	show->add_child(show_alias);
	std::shared_ptr<CommandTree> show_alias_source{
	    new CommandTreeAction("source", [this](std::unordered_map<std::string, std::string> args) {
		    auto mail_parts = Util::split(args.at("E-Mail"), '@');
		    try {
			    std::vector<models::Alias> aliases =
			        vmail.get_aliases_by_source(mail_parts.at(0), mail_parts.at(1));

			    std::cout << args.at("E-Mail") << " has the destinations:\n";
			    for (const auto& a : aliases) {
				    std::cout << a.id() << '\t' << a.destination_username() << '@'
				              << a.destination_domain();
				    if (a.enabled()) {
					    std::cout << std::endl;
				    } else {
					    std::cout << " (disabled)\n";
				    }
			    }
		    }
		    catch (std::out_of_range&) {
			    cli_error("Invalid E-Mail-Address!\n");
		    }
		    catch (std::runtime_error&) {
			    cli_error("Whoopsie!\n");
		    }
	    })};
	show_alias->add_child(show_alias_source);
	std::shared_ptr<CommandTree> show_alias_source_email{new CommandTreeParameter("E-Mail")};
	show_alias_source->add_child(show_alias_source_email);
	std::shared_ptr<CommandTree> show_alias_destination{new CommandTreeAction(
	    "destination", [this](std::unordered_map<std::string, std::string> args) {
		    auto mail_parts = Util::split(args.at("E-Mail"), '@');
		    try {
			    std::vector<models::Alias> aliases =
			        vmail.get_aliases_by_destination(mail_parts.at(0), mail_parts.at(1));

			    std::cout << args.at("E-Mail") << " has the sources:\n";
			    for (const auto& a : aliases) {
				    std::cout << a.id() << '\t' << a.source_username() << '@'
				              << a.source_domain().domain();
				    if (a.enabled()) {
					    std::cout << std::endl;
				    } else {
					    std::cout << " (disabled)\n";
				    }
			    }
		    }
		    catch (std::out_of_range&) {
			    cli_error("Invalid E-Mail-Address!\n");
		    }
		    catch (std::runtime_error&) {
			    cli_error("Whoopsie!\n");
		    }
	    })};
	std::shared_ptr<CommandTree> show_alias_destination_email{new CommandTreeParameter("E-Mail")};
	show_alias_destination->add_child(show_alias_destination_email);
	show_alias->add_child(show_alias_destination);

	std::unique_ptr<CommandTree> help{new CommandTreeAction(
	    "help", [this](std::unordered_map<std::string, std::string> args) { print_usage(); })};

	std::unique_ptr<CommandTree> exit{new CommandTreeAction(
	    "exit", [this](std::unordered_map<std::string, std::string> args) { run = false; })};

	command_trees.insert_or_assign("create", std::move(create));
	command_trees.insert_or_assign("delete", std::move(del));
	command_trees.insert_or_assign("show", std::move(show));
	command_trees.insert_or_assign("help", std::move(help));
	command_trees.insert_or_assign("exit", std::move(exit));
}

void Cli::print_usage() const
{
	std::cout << "Usage:\n\n";
	for (auto& command : command_trees) {
		std::cout << command.second->get_syntax() << std::endl;
	}
}
