#pragma once

#include <memory>
#include <optional>
#include <string>
#include <unordered_map>
#include <variant>
#include <vector>

class VMail;
class CommandTree;

class Cli
{
	//using result_line = std::variant<VMailUser, VMailAlias, VMailPolicy>;
	//using result = std::vector<result_line>;

private:
	VMail& vmail;
	//std::optional<result> last_result;

	std::unordered_map<std::string, std::unique_ptr<CommandTree>> command_trees;

	bool run{true};

public:
	explicit Cli(VMail& vmail);
	~Cli();

	void run_cli();

private:
	const char* get_prompt() const;

	void cli_error(const std::string& msg) const;
	void cli_info(const std::string& msg) const;
	void cli_success(const std::string& msg) const;

	void setup_command_trees();

	void print_usage() const;
};
