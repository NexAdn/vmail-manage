#pragma once

#include <exception>
#include <functional>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

class CommandTree
{
public:
	using argument_map_t = std::unordered_map<std::string, std::string>;
	using executor_t = std::function<void(argument_map_t)>;
	using vector_iterator_t = std::vector<std::string>::const_iterator;

protected:
	std::string label;
	std::weak_ptr<CommandTree> parent;
	std::vector<std::shared_ptr<CommandTree>> children;

public:
	CommandTree() = default;
	virtual ~CommandTree();

	virtual void add_child(std::shared_ptr<CommandTree> child);
	void execute(const std::vector<std::string>& input) const;

	virtual std::string get_syntax() const = 0;
	std::string get_label() const;

	bool is_leaf() const;
	bool has_parameter() const;

	virtual bool execute(vector_iterator_t input_iterator, vector_iterator_t input_end,
	                     argument_map_t& collected_arguments) const = 0;
};

class CommandTreeAction : public CommandTree
{
private:
	executor_t executor;

public:
	CommandTreeAction(const std::string& label);
	CommandTreeAction(const std::string& label, executor_t executor);
	~CommandTreeAction();

	std::string get_syntax() const override;

	bool execute(vector_iterator_t input_iterator, vector_iterator_t input_end,
	             argument_map_t& collected_arguments) const override;
};

class CommandTreeParameter : public CommandTree
{
public:
	CommandTreeParameter(const std::string& label);
	~CommandTreeParameter();

	std::string get_syntax() const;

	bool execute(vector_iterator_t input_iterator, vector_iterator_t input_end,
	             argument_map_t& collected_arguments) const override;
};

class CommandTreeException : public std::exception
{
private:
	std::string reason;

public:
	CommandTreeException(const std::string& reason);
	virtual ~CommandTreeException();

	const char* what() const noexcept;
};
