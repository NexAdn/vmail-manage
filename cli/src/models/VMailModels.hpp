#pragma once

#include <cstddef>
#include <cstdint>
#include <optional>
#include <string>
#include <variant>

#include "NotifyWrapper.hpp"

class VMail;

namespace models
{
class Domain
{
private:
	const VMail& vmail;

	size_t r_id;
	std::string r_domain;

public:
	Domain(const VMail& vmail, const size_t id, const std::string& domain)
	    : vmail(vmail), r_id(id), r_domain(domain)
	{}

	inline const size_t id() const
	{
		return r_id;
	}

	inline const std::string& domain() const
	{
		return r_domain;
	}
};

class Account
{
private:
	const VMail& vmail;

	size_t r_id;
	std::string r_username;
	Domain r_domain;
	std::string r_password;
	size_t r_quota;
	bool r_enabled;
	bool r_sendonly;

public:
	Account(const VMail& vmail, const size_t id, const std::string& username, Domain& domain,
	        const std::string& password, const size_t quota, const bool enabled,
	        const bool sendonly)
	    : vmail(vmail)
	    , r_id(id)
	    , r_username(username)
	    , r_domain(domain)
	    , r_password(password)
	    , r_quota(quota)
	    , r_enabled(enabled)
	    , r_sendonly(sendonly)
	{}

	inline const size_t id() const
	{
		return r_id;
	}

	inline const std::string& username() const
	{
		return r_username;
	}

	NotifyWrapper<std::string> username();

	const Domain& domain() const
	{
		return r_domain;
	}

	const std::string& password() const
	{
		return r_password;
	};

	NotifyWrapper<std::string> password();

	const size_t quota() const
	{
		return r_quota;
	}

	NotifyWrapper<size_t> quota();

	const bool enabled() const
	{
		return r_enabled;
	}

	NotifyWrapper<bool> enabled();

	const bool sendonly() const
	{
		return r_sendonly;
	}

	NotifyWrapper<bool> sendonly();
};

class Alias
{
private:
	const VMail& vmail;

	size_t r_id;
	std::string r_source_username;
	Domain r_source_domain;
	std::string r_destination_username;
	std::string r_destination_domain;
	bool r_enabled;

public:
	Alias(const VMail& vmail, size_t id, const std::string& source_username, Domain& source_domain,
	      const std::string& destination_username, const std::string& destination_domain,
	      const bool enabled)
	    : vmail(vmail)
	    , r_id(id)
	    , r_source_username(source_username)
	    , r_source_domain(source_domain)
	    , r_destination_username(destination_username)
	    , r_destination_domain(destination_domain)
	    , r_enabled(enabled)
	{}

	inline const size_t id() const
	{
		return r_id;
	}

	inline const std::string& source_username() const
	{
		return r_source_username;
	}

	inline const Domain& source_domain() const
	{
		return r_source_domain;
	}

	inline const std::string& destination_username() const
	{
		return r_destination_username;
	}

	inline const std::string& destination_domain() const
	{
		return r_destination_domain;
	}

	inline const bool enabled() const
	{
		return r_enabled;
	}
};
//
//
// enum class TLSPolicyType : int
//{
//	NONE,
//	MAY,
//	ENCRYPT,
//	DANE,
//	DANE_ONLY,
//	FINGERPRINT,
//	VERIFY,
//	SECURE
//};
//
// class TLSPolicy
//{
//	size_t id;
//	std::string domain;
//	TLSPolicyType policy;
//	std::optional<std::string> params;
//};
//
} // namespace models
