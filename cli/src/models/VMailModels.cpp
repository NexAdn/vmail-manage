#include "models/VMailModels.hpp"
#include "VMail.hpp"

namespace
{
template <typename V, typename O>
inline NotifyWrapper<V> update_wrapper(V& ref_var, const O& obj, const VMail& vmail)
{
	return NotifyWrapper<V>(ref_var, [&obj, &vmail](const V&) { vmail.update(obj); });
}
} // namespace

namespace models
{
NotifyWrapper<std::string> Account::username()
{
	return update_wrapper<std::string>(r_username, *this, vmail);
}

NotifyWrapper<std::string> Account::password()
{
	return update_wrapper<std::string>(r_password, *this, vmail);
}

NotifyWrapper<size_t> Account::quota()
{
	return update_wrapper<size_t>(r_quota, *this, vmail);
}

NotifyWrapper<bool> Account::enabled()
{
	return update_wrapper<bool>(r_enabled, *this, vmail);
}

NotifyWrapper<bool> Account::sendonly()
{
	return update_wrapper<bool>(r_sendonly, *this, vmail);
}
} // namespace models
