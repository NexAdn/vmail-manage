#include <string>

#include <cgreen/cgreen.h>

#include "test.hpp"

#include "NotifyWrapper.hpp"

Describe(NotifyWrapper);
BeforeEach(NotifyWrapper)
{}

AfterEach(NotifyWrapper)
{}

Ensure(NotifyWrapper, calls_updater)
{
	bool updater_called{false};
	std::string wrapped_value{"hi"};
	std::string expected_new_value{"ho"};
	NotifyWrapper<std::string> nw(
	    wrapped_value, [&updater_called, &expected_new_value](const std::string& new_value) {
		    if (new_value == expected_new_value) {
			    updater_called = true;
		    }
	    });

	nw = expected_new_value;

	cgreen::assert_that(updater_called, is_true);
}

Ensure(NotifyWrapper, returns_value)
{
	std::string wrapped_value{"hi"};
	NotifyWrapper<std::string> nw(wrapped_value);

	std::string copy = nw;

	cgreen::assert_that(copy.c_str(), cgreen::is_equal_to_string(wrapped_value.c_str()));
}

MAKE_BEGIN_TEST_MAIN

cgreen::add_test_with_context(suite, NotifyWrapper, calls_updater);
cgreen::add_test_with_context(suite, NotifyWrapper, returns_value);

MAKE_END_TEST_MAIN
