#include <iostream>
#include <memory>

#include <readline/history.h>
#include <readline/readline.h>

#include "CommandTree.hpp"
#include "Util.hpp"

int main(int argc, char** argv)
{
	bool continue_loop{true};

	std::unique_ptr<CommandTree> root{new CommandTreeAction("root")};

	std::shared_ptr<CommandTree> root_help{
	    new CommandTreeAction("help", [&root](CommandTree::argument_map_t) {
		    std::cout << root->get_syntax() << std::endl;
	    })};
	std::shared_ptr<CommandTree> root_echo{
	    new CommandTreeAction("echo", [](CommandTree::argument_map_t args) {
		    std::cout << args.at("echo_param") << std::endl;
	    })};
	std::shared_ptr<CommandTree> root_echo_param{new CommandTreeParameter("echo_param")};
	std::shared_ptr<CommandTree> root_double{new CommandTreeAction("double")};
	std::shared_ptr<CommandTree> root_double_echo{
	    new CommandTreeAction("echo", [](CommandTree::argument_map_t args) {
		    std::cout << args.at("echo_param") << std::endl << args.at("echo_param") << std::endl;
	    })};
	std::shared_ptr<CommandTree> root_exit{new CommandTreeAction(
	    "exit", [&continue_loop](CommandTree::argument_map_t) { continue_loop = false; })};

	root->add_child(root_help);
	root->add_child(root_echo);
	root_echo->add_child(root_echo_param);
	root->add_child(root_double);
	root_double->add_child(root_double_echo);
	root_double_echo->add_child(root_echo_param);
	root->add_child(root_exit);

	while (continue_loop) {
		char* line = readline("testcmd> ");

		auto split_line = Util::split(std::string("root ") + line);
		free(line);

		try {
			root->execute(split_line);
		}
		catch (CommandTreeException& e) {
			std::cout << "Command execution failed\n";
		}
	}
}
