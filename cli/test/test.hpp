#ifndef MAKE_TEST_HPP__
#define MAKE_TEST_HPP__
#pragma once

#define MAKE_BEGIN_TEST_MAIN        \
	int main(int argc, char** argv) \
	{                               \
		TestSuite* suite = cgreen::create_test_suite();

#define MAKE_END_TEST_MAIN                                                              \
	if (argc > 1)                                                                       \
	{                                                                                   \
		return cgreen::run_single_test(suite, argv[1], cgreen::create_text_reporter()); \
	}                                                                                   \
	return cgreen::run_test_suite(suite, cgreen::create_text_reporter());               \
	}

#endif // MAKE_TEST_HPP__
