#include <fstream>
#include <streambuf>
#include <string>

#include <iostream>

#include <sqlpp11/mysql/mysql.h>
#include <sqlpp11/sqlpp11.h>

#include <cgreen/cgreen.h>

#include "VMail.hpp"
#include "vmail_sql.h"

#include "config.h"
#include "test.hpp"

namespace
{
constexpr const char* DB_SETUP_SQL_PATH{VMAIL_MANAGE_ROOT_DIR "/res/testdb.sql"};

auto conf = std::make_shared<sqlpp::mysql::connection_config>();
} // namespace

Describe(VMail);
BeforeEach(VMail)
{
	conf->host = "localhost";
	conf->port = 3306;
	conf->user = "vmail";
	conf->password = "vmail";
	conf->database = "vmail";

	sqlpp::mysql::connection db(conf);

	db.start_transaction();

	tbl::Accounts accounts;
	tbl::Domains domains;

	// std::ifstream setup_sql_file(DB_SETUP_SQL_PATH);
	// std::string setup_sql{std::istreambuf_iterator<char>(setup_sql_file),
	//                      std::istreambuf_iterator<char>()};

	// std::cout << "====== SQL ======\n" << setup_sql << std::endl << "====== /SQL ======\n";

	// db.execute(setup_sql);

	db(remove_from(accounts).where(accounts.domain == "testdomain"));
	db(remove_from(domains).where(domains.domain == "testdomain"));
	db(insert_into(domains).set(domains.domain = "testdomain"));
	db(insert_into(accounts).set(accounts.username = "testuser", accounts.password = "testpassword",
	                             accounts.domain = "testdomain"));

	db.commit_transaction();
}
AfterEach(VMail)
{
	sqlpp::mysql::connection db(conf);

	tbl::Accounts accounts;
	tbl::Domains domains;
	tbl::Aliases aliases;

	db.start_transaction();

	db(remove_from(aliases).where(aliases.id > 0));
	db(remove_from(accounts).where(accounts.id > 2));
	db(remove_from(domains).where(domains.id > 1));

	db.commit_transaction();
}

Ensure(VMail, finds_user)
{
	VMail vmail("vmail", "vmail", "vmail");

	const auto acc = vmail.get_account("testuser", "testdomain");

	cgreen::assert_that(acc.username().c_str(), cgreen::is_equal_to_string("testuser"));
	cgreen::assert_that(acc.domain().domain().c_str(), cgreen::is_equal_to_string("testdomain"));
	cgreen::assert_that(acc.password().c_str(), cgreen::is_equal_to_string("testpassword"));
	cgreen::assert_that(acc.id(), cgreen::is_greater_than(0));
}

Ensure(VMail, throws_on_user_not_found)
{
	VMail vmail("vmail", "vmail", "vmail");

	try {
		const auto acc = vmail.get_account("invaliduser", "testdomain");
		cgreen::assert_that(true, is_false);
	}
	catch (const std::runtime_error&) {
		cgreen::assert_that(true, is_true);
	}
}

Ensure(VMail, finds_domain_by_id)
{
	VMail vmail("vmail", "vmail", "vmail");

	const auto domain = vmail.get_domain(1);

	cgreen::assert_that(domain.id(), cgreen::is_equal_to(1));
}

Ensure(VMail, throws_on_domain_by_id_not_found)
{
	VMail vmail("vmail", "vmail", "vmail");

	try {
		vmail.get_domain(0);
	}
	catch (const std::runtime_error&) {
		cgreen::assert_that(true, is_true);
	}
}

Ensure(VMail, finds_domain_by_name)
{
	VMail vmail("vmail", "vmail", "vmail");

	const auto domain = vmail.get_domain("testdomain");

	cgreen::assert_that(domain.domain().c_str(), cgreen::is_equal_to_string("testdomain"));
	cgreen::assert_that(domain.id(), cgreen::is_greater_than(0));
}

Ensure(VMail, throws_on_domain_by_name_not_found)
{
	VMail vmail("vmail", "vmail", "vmail");

	try {
		vmail.get_domain("invaliddomain");
	}
	catch (const std::runtime_error&) {
		cgreen::assert_that(true, is_true);
	}
}

Ensure(VMail, creates_account)
{
	VMail vmail("vmail", "vmail", "vmail");

	models::Account account =
	    vmail.create_account("newuser", vmail.get_domain("testdomain"), "passhash");
	const auto& c_account{account};

	cgreen::assert_that(c_account.id(), cgreen::is_greater_than(0));
	cgreen::assert_that(c_account.username().c_str(), cgreen::is_equal_to_string("newuser"));
	cgreen::assert_that(c_account.domain().domain().c_str(),
	                    cgreen::is_equal_to_string("testdomain"));
	cgreen::assert_that(c_account.password().c_str(), cgreen::is_equal_to_string("passhash"));
}

Ensure(VMail, creates_domain)
{
	VMail vmail("vmail", "vmail", "vmail");

	models::Domain domain = vmail.create_domain("newdomain");
	const auto& c_domain{domain};

	cgreen::assert_that(c_domain.id(), cgreen::is_greater_than(0));
	cgreen::assert_that(c_domain.domain().c_str(), cgreen::is_equal_to_string("newdomain"));
}

Ensure(VMail, updates_account)
{
	VMail vmail("vmail", "vmail", "vmail");

	models::Account account = vmail.get_account("testuser", "testdomain");
	account.username() = "newuser";

	models::Account new_account = vmail.get_account("newuser", "testdomain");

	cgreen::assert_that(new_account.id(), cgreen::is_equal_to(account.id()));
}

Ensure(VMail, creates_aliases)
{
	VMail vmail("vmail", "vmail", "vmail");

	models::Domain domain = vmail.get_domain("testdomain");

	models::Alias alias = vmail.create_alias("testalias", domain, "newalias", "newdomain");

	cgreen::assert_that(alias.id(), cgreen::is_greater_than(0));
	cgreen::assert_that(alias.source_username().c_str(), cgreen::is_equal_to_string("testalias"));
	cgreen::assert_that(alias.source_domain().id(), cgreen::is_equal_to(domain.id()));
	cgreen::assert_that(alias.destination_username().c_str(),
	                    cgreen::is_equal_to_string("newalias"));
	cgreen::assert_that(alias.destination_domain().c_str(),
	                    cgreen::is_equal_to_string("newdomain"));
}

Ensure(VMail, gets_aliases_by_source)
{
	VMail vmail("vmail", "vmail", "vmail");

	models::Domain domain = vmail.get_domain("testdomain");

	vmail.create_alias("testalias", domain, "newalias1", "newdomain");
	vmail.create_alias("testalias", domain, "newalias2", "newdomain");

	std::vector<models::Alias> found_aliases = vmail.get_aliases_by_source("testalias", domain);

	cgreen::assert_that(found_aliases.size(), cgreen::is_equal_to(2));
	for (const auto& a : found_aliases) {
		cgreen::assert_that(a.source_username().c_str(), cgreen::is_equal_to_string("testalias"));
		cgreen::assert_that(a.source_domain().domain().c_str(),
		                    cgreen::is_equal_to_string(domain.domain().c_str()));
		cgreen::assert_that(a.destination_domain().c_str(),
		                    cgreen::is_equal_to_string("newdomain"));
	}
}

Ensure(VMail, gets_aliases_by_destination)
{
	VMail vmail("vmail", "vmail", "vmail");

	models::Domain domain = vmail.get_domain("testdomain");

	vmail.create_alias("testalias1", domain, "newalias", "newdomain");
	vmail.create_alias("testalias2", domain, "newalias", "newdomain");

	std::vector<models::Alias> found_aliases =
	    vmail.get_aliases_by_destination("newalias", "newdomain");

	cgreen::assert_that(found_aliases.size(), cgreen::is_equal_to(2));
	for (const auto& a : found_aliases) {
		cgreen::assert_that(a.destination_username().c_str(),
		                    cgreen::is_equal_to_string("newalias"));
		cgreen::assert_that(a.destination_domain().c_str(),
		                    cgreen::is_equal_to_string("newdomain"));
		cgreen::assert_that(a.source_domain().domain().c_str(),
		                    cgreen::is_equal_to_string(domain.domain().c_str()));
	}
}

MAKE_BEGIN_TEST_MAIN

cgreen::add_test_with_context(suite, VMail, finds_user);
cgreen::add_test_with_context(suite, VMail, throws_on_user_not_found);
cgreen::add_test_with_context(suite, VMail, finds_domain_by_name);
cgreen::add_test_with_context(suite, VMail, throws_on_domain_by_name_not_found);
cgreen::add_test_with_context(suite, VMail, finds_domain_by_id);
cgreen::add_test_with_context(suite, VMail, throws_on_domain_by_id_not_found);
cgreen::add_test_with_context(suite, VMail, creates_account);
cgreen::add_test_with_context(suite, VMail, creates_domain);
cgreen::add_test_with_context(suite, VMail, updates_account);
cgreen::add_test_with_context(suite, VMail, creates_aliases);
cgreen::add_test_with_context(suite, VMail, gets_aliases_by_source);
cgreen::add_test_with_context(suite, VMail, gets_aliases_by_destination);

MAKE_END_TEST_MAIN
