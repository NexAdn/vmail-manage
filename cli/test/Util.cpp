#include "test.hpp"

#include <cgreen/cgreen.h>

#include "Util.hpp"

Describe(Util);
BeforeEach(Util)
{}
AfterEach(Util)
{}

Ensure(Util, split_splits_string_by_space)
{
	std::string input{"hello world"};

	auto res = Util::split(input, ' ');

	cgreen::assert_that(res.size(), cgreen::is_equal_to(2));
	cgreen::assert_that(res.at(0).c_str(), cgreen::is_equal_to_string("hello"));
	cgreen::assert_that(res.at(1).c_str(), cgreen::is_equal_to_string("world"));
}

Ensure(Util, split_returns_empty_array_on_empty_string)
{
	std::string input{""};

	auto res = Util::split(input);

	cgreen::assert_that(res.size(), cgreen::is_equal_to(0));
}

Ensure(Util, split_removes_empty_segments)
{
	std::string input{"before  after"};

	auto res = Util::split(input);

	cgreen::assert_that(res.size(), cgreen::is_equal_to(2));
	cgreen::assert_that(res.at(0).c_str(), cgreen::is_equal_to_string("before"));
	cgreen::assert_that(res.at(1).c_str(), cgreen::is_equal_to_string("after"));
}

MAKE_BEGIN_TEST_MAIN

cgreen::add_test_with_context(suite, Util, split_splits_string_by_space);
cgreen::add_test_with_context(suite, Util, split_returns_empty_array_on_empty_string);
cgreen::add_test_with_context(suite, Util, split_removes_empty_segments);

MAKE_END_TEST_MAIN
