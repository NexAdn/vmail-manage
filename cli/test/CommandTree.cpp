#include <iostream>

#include "test.hpp"

#include <cgreen/cgreen.h>

#include "CommandTree.hpp"
#include "Util.hpp"

Describe(CommandTree);
BeforeEach(CommandTree)
{}
AfterEach(CommandTree)
{}

Ensure(CommandTree, builds_empty_children_syntax)
{
	CommandTreeAction root("root");

	cgreen::assert_that(root.get_syntax().c_str(), cgreen::is_equal_to_string("root"));
}

Ensure(CommandTree, builds_single_children_syntax)
{
	CommandTreeAction root("root");
	std::shared_ptr<CommandTree> act1{new CommandTreeAction("act1")};
	root.add_child(act1);

	cgreen::assert_that(root.get_syntax().c_str(), cgreen::is_equal_to_string("root act1"));
}

Ensure(CommandTree, builds_complex_syntax)
{
	CommandTreeAction root("root");

	std::shared_ptr<CommandTree> act1{new CommandTreeAction("act1")};
	root.add_child(act1);
	std::shared_ptr<CommandTree> act2{new CommandTreeAction("act2")};
	root.add_child(act2);

	std::shared_ptr<CommandTree> param1{new CommandTreeParameter("param1")};
	act1->add_child(param1);
	std::shared_ptr<CommandTree> param2_act{new CommandTreeAction("param2")};
	act2->add_child(param2_act);
	std::shared_ptr<CommandTree> param2{new CommandTreeParameter("param2")};
	param2_act->add_child(param2);
	std::shared_ptr<CommandTree> param3_act{new CommandTreeAction("param3")};
	act2->add_child(param3_act);
	std::shared_ptr<CommandTree> param3{new CommandTreeParameter("param3")};
	param3_act->add_child(param3);

	cgreen::assert_that(
	    root.get_syntax().c_str(),
	    cgreen::is_equal_to_string("root [act1 <param1>|act2 [param2 <param2>|param3 <param3>]]"));
}

Ensure(CommandTree, uses_correct_executor)
{
	bool executed{false};

	std::unique_ptr<CommandTree> root{new CommandTreeAction("root")};
	std::shared_ptr<CommandTreeAction> act1{new CommandTreeAction("act1")};
	std::shared_ptr<CommandTreeAction> act2{new CommandTreeAction("act2")};
	root->add_child(act1);
	root->add_child(act2);

	std::shared_ptr<CommandTreeAction> act3{new CommandTreeAction(
	    "act3", [&executed](CommandTree::argument_map_t args) { executed = true; })};
	act2->add_child(act3);

	root->execute(Util::split("root act2 act3"));

	cgreen::assert_that(act3->is_leaf(), is_true);
	cgreen::assert_that(executed, is_true);
}

Ensure(CommandTree, parses_parameters)
{
	bool parsed_correctly{false};

	std::unique_ptr<CommandTree> root{std::unique_ptr<CommandTree>(
	    new CommandTreeAction("root", [&parsed_correctly](CommandTree::argument_map_t args) {
		    parsed_correctly = args.at("param1") == "value1";
	    }))};
	std::shared_ptr<CommandTreeParameter> param1{new CommandTreeParameter("param1")};

	root->add_child(param1);

	root->execute(Util::split("root value1"));

	cgreen::assert_that(parsed_correctly, is_true);
}

Ensure(CommandTree, detects_if_has_parameter)
{
	std::unique_ptr<CommandTree> root{new CommandTreeAction("root")};

	cgreen::assert_that(root->has_parameter(), is_true);

	root->add_child(std::unique_ptr<CommandTree>(new CommandTreeParameter("param1")));

	cgreen::assert_that(root->has_parameter(), is_false);
}

Ensure(CommandTree, parameter_child_is_always_only_child)
{
	std::unique_ptr<CommandTree> root{new CommandTreeAction("root")};
	std::shared_ptr<CommandTree> param1{new CommandTreeParameter("param1")};
	std::shared_ptr<CommandTree> act1{new CommandTreeAction("act1")};
	std::shared_ptr<CommandTreeParameter> param2{new CommandTreeParameter("param2")};

	root->add_child(param1);

	bool threw_exception;

	try {
		root->add_child(act1);
		threw_exception = false;
	}
	catch (std::exception& e) {
		threw_exception = true;
	}
	cgreen::assert_that(threw_exception, is_true);

	try {
		root->add_child(param2);
		threw_exception = false;
	}
	catch (CommandTreeException& e) {
		threw_exception = true;
	}
	cgreen::assert_that(threw_exception, is_true);
}

Ensure(CommandTree, execution_is_passed_on_if_parameter_is_no_leaf)
{
	bool root_called{false};
	bool act1_called{false};

	std::unique_ptr<CommandTree> root{new CommandTreeAction(
	    "root", [&root_called](CommandTree::argument_map_t) { root_called = true; })};
	std::shared_ptr<CommandTree> param1{new CommandTreeParameter("param1")};
	std::shared_ptr<CommandTree> act1{new CommandTreeAction(
	    "act1", [&act1_called](CommandTree::argument_map_t) { act1_called = true; })};

	root->add_child(param1);
	param1->add_child(act1);

	root->execute(Util::split("root value act1"));

	cgreen::assert_that(root_called, is_false);
	cgreen::assert_that(act1_called, is_true);
}

Ensure(CommandTree, detects_if_leaf)
{
	std::unique_ptr<CommandTree> root{new CommandTreeAction("root")};
	std::shared_ptr<CommandTree> child{new CommandTreeAction("child")};
	root->add_child(child);

	cgreen::assert_that(root->is_leaf(), is_false);
	cgreen::assert_that(child->is_leaf(), is_true);
}

Ensure(CommandTree, handles_too_short_input)
{
	std::unique_ptr<CommandTree> root{new CommandTreeAction("root")};
	std::shared_ptr<CommandTree> act1{new CommandTreeAction("act1")};
	std::shared_ptr<CommandTree> act2{new CommandTreeAction("act2")};
	std::shared_ptr<CommandTree> param1{new CommandTreeParameter("param1")};

	root->add_child(act1);
	root->add_child(act2);
	act1->add_child(param1);

	bool threw;

	try {
		root->execute(Util::split("root"));
		threw = false;
	}
	catch (CommandTreeException& e) {
		threw = true;
	}
	catch (std::exception& e) {
		std::cout << "Threw wrong exception:\n" << e.what() << std::endl;
		threw = false;
	}

	cgreen::assert_that(threw, is_true);

	try {
		root->execute(Util::split("root act1"));
		threw = false;
	}
	catch (CommandTreeException& e) {
		threw = true;
	}
	catch (std::exception& e) {
		std::cout << "Threw wrong exception:\n" << e.what() << std::endl;
		threw = false;
	}
}

MAKE_BEGIN_TEST_MAIN

cgreen::add_test_with_context(suite, CommandTree, builds_empty_children_syntax);
cgreen::add_test_with_context(suite, CommandTree, builds_single_children_syntax);
cgreen::add_test_with_context(suite, CommandTree, builds_complex_syntax);
cgreen::add_test_with_context(suite, CommandTree, uses_correct_executor);
cgreen::add_test_with_context(suite, CommandTree, parses_parameters);
cgreen::add_test_with_context(suite, CommandTree, parameter_child_is_always_only_child);
cgreen::add_test_with_context(suite, CommandTree, execution_is_passed_on_if_parameter_is_no_leaf);
cgreen::add_test_with_context(suite, CommandTree, detects_if_leaf);
cgreen::add_test_with_context(suite, CommandTree, handles_too_short_input);

MAKE_END_TEST_MAIN
