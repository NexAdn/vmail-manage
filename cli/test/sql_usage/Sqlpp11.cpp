#include <memory>
#include <string>

#include <sqlpp11/mysql/mysql.h>
#include <sqlpp11/sqlpp11.h>

#include <cgreen/cgreen.h>

#include "test.hpp"
#include "vmail_sql.h"

auto conn_config = std::make_shared<sqlpp::mysql::connection_config>();

std::unique_ptr<sqlpp::mysql::connection> conn;

Describe(Sqlpp11);
BeforeEach(Sqlpp11)
{
	conn_config->host = "localhost";
	conn_config->port = 3306;
	conn_config->user = "vmail";
	conn_config->password = "vmail";
	conn_config->database = "vmail";
	conn_config->debug = true;

	conn = std::make_unique<sqlpp::mysql::connection>(conn_config);
}
AfterEach(Sqlpp11)
{
	conn.reset();
}

Ensure(Sqlpp11, queries)
{
	tbl::Accounts acc_table;
	for (const auto& row : (*conn)(select(acc_table.username, acc_table.domain)
	                                   .from(acc_table)
	                                   .where(acc_table.username == "nex"))) {
		cgreen::assert_that(static_cast<std::string>(row.username).c_str(),
		                    cgreen::is_equal_to_string("nex"));
	}
}

Ensure(Sqlpp11, inserts_and_deletes)
{
	tbl::Accounts acc_table;
	auto res = (*conn)(insert_into(acc_table).set(acc_table.username = "testuser",
	                                              acc_table.domain = "nexadn.de",
	                                              acc_table.password = "testpass"));
	cgreen::assert_that(res, cgreen::is_greater_than(0));

	res = (*conn)(remove_from(acc_table).where(acc_table.username == "testuser"));
	cgreen::assert_that(res, cgreen::is_greater_than(0));
}

Ensure(Sqlpp11, updates)
{
	tbl::Accounts acc_table;

	conn->start_transaction();

	auto res = (*conn)(insert_into(acc_table).set(acc_table.username = "testuser",
	                                              acc_table.domain = "nexadn.de",
	                                              acc_table.password = "testpass"));

	cgreen::assert_that(res, cgreen::is_greater_than(0));

	res = (*conn)(update(acc_table)
	                  .set(acc_table.username = "invalid")
	                  .where(acc_table.username == "testuser"));

	cgreen::assert_that(res, cgreen::is_greater_than(0));

	conn->rollback_transaction(false);
}

MAKE_BEGIN_TEST_MAIN

cgreen::add_test_with_context(suite, Sqlpp11, queries);
cgreen::add_test_with_context(suite, Sqlpp11, inserts_and_deletes);
cgreen::add_test_with_context(suite, Sqlpp11, updates);

MAKE_END_TEST_MAIN