DROP TABLE IF EXISTS `accounts`;
CREATE TABLE `accounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `domain` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `quota` int(10) unsigned DEFAULT 0,
  `enabled` tinyint(1) DEFAULT 0,
  `sendonly` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`,`domain`),
  KEY `domain` (`domain`),
  CONSTRAINT `accounts_ibfk_1` FOREIGN KEY (`domain`) REFERENCES `domains` (`domain`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;
LOCK TABLES `accounts` WRITE;
UNLOCK TABLES;
DROP TABLE IF EXISTS `aliases`;
CREATE TABLE `aliases` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source_username` varchar(64) NOT NULL,
  `source_domain` varchar(255) NOT NULL,
  `destination_username` varchar(64) NOT NULL,
  `destination_domain` varchar(255) NOT NULL,
  `enabled` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `source_username` (`source_username`,`source_domain`,`destination_username`,`destination_domain`),
  KEY `source_domain` (`source_domain`),
  CONSTRAINT `aliases_ibfk_1` FOREIGN KEY (`source_domain`) REFERENCES `domains` (`domain`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
LOCK TABLES `aliases` WRITE;
UNLOCK TABLES;
DROP TABLE IF EXISTS `domains`;
CREATE TABLE `domains` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `domain` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `domain` (`domain`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
LOCK TABLES `domains` WRITE;
INSERT INTO `domains` VALUES (1,'nexadn.de');
UNLOCK TABLES;
DROP TABLE IF EXISTS `tlspolicies`;
CREATE TABLE `tlspolicies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `domain` varchar(255) NOT NULL,
  `policy` enum('none','may','encrypt','dane','dane-only','fingerprint','verify','secure') NOT NULL,
  `params` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ky` (`domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
LOCK TABLES `tlspolicies` WRITE;
UNLOCK TABLES;
