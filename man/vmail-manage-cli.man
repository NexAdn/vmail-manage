.TH "VMAIL-MANAGE-CLI" 1 2020-03-09 Linux "vmail-manage"

.SH NAME
vmail-manage \- The
.BR vmail-manage(1)
CLI interface

.SH SYNOPSIS

.SH DESCRIPTION

.SH OPTIONS

.SH SEE ALSO

.BR vmail-manage(1)
.BR vmail-manage-tui(1)
