.TH "VMAIL-MANAGE" 1 2020-03-09 Linux "vmail-manage"

.SH NAME
vmail-manage \- A terminal management interface for vmail database

.SH SYNOPSIS
.RS 2

vmail-manage [-c|-t]

.RE

.SH DESCRIPTION

.SH OPTIONS

.SS -c

Start the CLI

.SS -t

Start the TUI

.SH SEE ALSO

.BR vmail-manage-cli(1)
.BR vmail-manage-tui(1)
